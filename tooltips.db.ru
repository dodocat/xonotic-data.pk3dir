0
\XonoticSingleplayerDialog\Начать одиночную кампанию или быструю игру против ботов


\XonoticMultiplayerDialog\Играть по сети, просмотреть демо или изменить настройки игрока
\XonoticMultiplayerDialog/Серверы\Поиск игровых серверов
\menu_slist_showempty\Показывать пустые сервера
\menu_slist_showfull\Показывать полные сервера, не имеющие свободных мест
\net_slist_pause\Приостановить обновление списка серверов для предотвращения их скакания
\XonoticMultiplayerDialog/Info...\Показать больше сведений о выбранном сервере
\XonoticMultiplayerDialog/В закладки\Добавить выбранный сервер в закладки, так найти его будет быстрее
\XonoticMultiplayerDialog/Создать\Запустить собственную игру
\XonoticMultiplayerDialog/Демо\Список демо для просмотра
\XonoticMultiplayerDialog/Player Setup\Изменить настройки игрока

\XonoticTeamSelectDialog/join 'best' team (auto-select)\Автовыбор команды (советуется)
\XonoticTeamSelectDialog/красная\Присоединиться к красной команде
\XonoticTeamSelectDialog/синяя\Присоединиться к синей команде
\XonoticTeamSelectDialog/жёлтая\Присоединиться к жёлтой команде
\XonoticTeamSelectDialog/розовая\Присоединиться к розовой команде

\timelimit_override\Ограничение времени в минутах, состязание закончится при его достижении
\fraglimit_override\Количество очков, необходимых для завершения состязания
\menu_maxplayers\Предельное количество игроков и ботов, которые могут быть одновременно подключены к серверу
\bot_number\Количество ботов на сервере
\skill\Насколько искусными будут боты
\g_maplist_votable\Количество карт, предлагаемых в голосовании после состязания
\sv_vote_simple_majority_factor\Простое большинство выигрывает голосование
\XonoticMultiplayerDialog/Advanced settings...\Дополнительные серверные настройки
\XonoticMultiplayerDialog/Mutators...\Мутаторы и арены оружий
\g_dodging\Enable dodging
\g_cloaked\Все игроки почти невидимы
\g_footsteps\Включить звуки шагов
\g_midair\Только находящиеся в воздухе получают повреждения
\g_vampire\Урон, наносимый противнику, прибавляется к собственному здоровью
\g_bloodloss\Величина здоровья, ниже которой наступает оглушение из-за потери крови
\sv_gravity\Падение происходит медленнее. Чем ниже значение, тем ниже гравитация
\g_grappling_hook\Передвижение и полёты на подтягивающем крюке
\g_jetpack\Полёты на реактивном ранце
\g_pinata\Во время смерти выбрасывается всё оружие, которое нёс "убитый", что даёт возможность его подобрать
\g_weapon_stay\Всё собранное оружие остаётся после возрождений
\g_weaponarena\Selecting a weapon arena will give all players that weapon at spawn as well as unlimited ammo, and disable all other weapon pickups.
\menu_weaponarena_with_laser\Also enable the laser in the weapon arena
\g_minstagib\Players will be given the Minstanex, which is a railgun with infinite damage. If the player runs out of ammo, he will have 10 seconds to find some or if he fails to do so, face death. The secondary fire mode is a laser which does not inflict any damage and is good for doing trickjumps.
\g_nix\No items Xonotic - instead of pickup items, everyone plays with the same weapon. After some time, a countdown will start, after which everyone will switch to another weapon.
\g_nix_with_laser\Always carry the laser as an additional weapon in Nix
\XonoticMultiplayerDialog/Select all\Выбрать все карты
\XonoticMultiplayerDialog/Select none\Снять выделение со всех карт


\XonoticMultiplayerDialog/Проверка производительности\Замерить, насколько быстро компьютер способен играть выбранное демо

\fov\Угол обзора в градусах, допустимы значения от 60 то 130, по умолчанию 90
\cl_bobcycle\Частота качания вида
\cl_zoomfactor\How big the zoom factor is when the zoom button is pressed
\cl_zoomsensitivity\How zoom changes sensitivity, from 0 (lower sensitivity) to 1 (no sensitivity change)
\cl_zoomspeed\How fast the view will be zoomed, disable to zoom instantly
\XonoticMultiplayerDialog/Weapon settings...\Set your most preferred weapons, autoswitch and weapon model settings

\cl_weaponpriority_useforcycling\Make use of the list above when cycling through weapons with the mouse wheel
\cl_autoswitch\Automatically switch to newly picked up weapons if they are better than what you are carrying
\r_drawviewmodel\Draw the weapon model
\cl_gunalign\Position of the weapon model; requires reconnect

\crosshair_per_weapon\Set a different crosshair for each weapon, good if you play without weapon models
\crosshair_color_per_weapon\Set the color of the crosshair depending on the weapon you are currently holding
\crosshair_size\Настроить размер перекрестья
\crosshair_color_alpha\Настроить прозрачность перекрестья
\crosshair_color\Adjust the crosshair color
\sbar_hudselector\Use the old HUD layout
\XonoticMultiplayerDialog/Waypoints setup...\-
\_cl_name\Имя, под которым вы появитесь в игре

\XonoticSettingsDialog\Изменить настройки игры
\XonoticCreditsDialog\The Xonotic credits
\XonoticTeamSelectDialog\-
\XonoticMutatorsDialog\-
\XonoticMapInfoDialog\-
\XonoticUserbindEditDialog\-
\XonoticWinnerDialog\-
\XonoticWeaponsDialog\-
\XonoticRadarDialog\-
\XonoticServerInfoDialog\-
\XonoticCvarsDialog\-

\XonoticQuitDialog\Выйти из игры
\XonoticQuitDialog/Да\Пора саночки возить...
\XonoticQuitDialog/Нет\Остались здесь ещё дела!

\XonoticSettingsDialog/Ввод\Настройки устройств ввода
\sensitivity\Множитель скорости мыши
\menu_mouse_speed\Множитель скорости мыши в меню, не влияет на прицеливание в игре
\m_filter\Сглаживает движения мыши, но значительно ухудшает отзывчивость прицеливания
\m_pitch\Обратить движения мыши по вертикальной оси
\vid_dgamouse\Использовать DGA ввод для мыши
\con_closeontoggleconsole\Использовать привязку для открытия консоли также и для её сокрытия

\XonoticSettingsDialog/Изображение\Настройки изображения
\vid_width\Разрешение экрана
\vid_bitsperpixel\Сколько бит на точку использовать для вывода, советуется 32
\vid_fullscreen\Включить полноэкранный режим (по умолчанию: включено)
\vid_vsync\Enable vertical synchronization to prevent tearing, will cap your fps to the screen refresh rate (default: disabled)
\gl_texture_anisotropy\Anisotropic filtering quality (default: 1x)
\r_glsl\Enable OpenGL 2.0 pixel shaders for lightning (default: enabled)
\gl_vbo\Make use of Vertex Buffer Objects to store static geometry in video memory for faster rendering (default: Vertex and Triangles)
\r_depthfirst\Eliminate overdraw by rendering a depth-only version of the scene before the normal rendering starts (default: disabled)
\gl_texturecompression\Compress the textures for video cards with small amounts of video memory available (default: None)
\gl_finish\Make the CPU wait for the GPU to finish each frame, can help with some strange input or video lag on some machines (default: disabled)
\v_brightness\Яркость чёрного (по умолчанию: 0)
\v_contrast\Яркость белого (по умолчанию: 1)
\v_gamma\Inverse gamma correction value, a brightness effect that does not affect white or black (default: 1.125)
\v_contrastboost\By how much to multiply the contrast in dark areas (default: 1)
\r_glsl_saturation\Saturation adjustment (0 = grayscale, 1 = normal, 2 = oversaturated), requires GLSL color control (default: 1)
\v_glslgamma\Enable use of GLSL to apply gamma correction, note that it might decrease performance by a lot (default: disabled)
\r_ambient\Окружающее освещение, если выставлено слишком сильным, приводит к тому, что свет на картах выглядит блёклым и плоским (по умолчанию: 4)
\r_hdr_scenebrightness\Общая яркость вывода (по умолчанию: 1)
\vid_samples\Enable antialiasing, which smooths the edges of 3D geometry. Note that it might decrease performance by quite a lot (default: disabled)
\v_flipped\Poor man's left handed mode (default: off)

\XonoticSettingsDialog/Эффекты\Настройки эффектов
\r_subdivisions_tolerance\Change the smoothness of the curves on the map (default: normal)
\gl_picmip\Change the sharpness of the textures. Lowering it will effectively reduce texture memory usage, but make the textures appear very blurry. (default: good)
\r_picmipworld\If set, only reduce the texture quality of models (default: enabled)
\mod_q3bsp_nolightmaps\Use high resolution lightmaps, which will look pretty but use up some extra video memory (default: enabled)
\cl_particles_quality\Multiplier for amount of particles. Less means less particles, which in turn gives for better performance (default: 1.0)
\r_drawparticles_drawdistance\Particles further away than this will not be drawn (default: 1000)
\cl_decals\Enable decals (bullet holes and blood) (default: enabled)
\r_drawdecals_drawdistance\Decals further away than this will not be drawn (default: 300)
\cl_decals_time\Time in seconds before decals fade away (default: 2)
\cl_gentle\Replace blood and gibs with content that does not have any gore effects (default: disabled)
\cl_nogibs\Reduce the amount of gibs or remove them completely (default: lots)
\v_kicktime\How long a view kick from damage lasts (default: 0)
\r_glsl_deluxemapping\Use per-pixel lighting effects (default: enabled)
\r_shadow_gloss\Enable the use of glossmaps on textures supporting it (default: enabled)
\gl_flashblend\Enable faster but uglier dynamic lights by rendering bright coronas instead of real dynamic lights (default: disabled)
\r_shadow_realtime_dlight\Enable rendering of dynamic lights such as explosions and rocket lights (default: enabled)
\r_shadow_realtime_dlight_shadows\Enable rendering of shadows from dynamic lights (default: disabled)
\r_shadow_realtime_world\Enable rendering of full realtime world lighting on maps that support it. Note that this might have a big impact on performance. (default: disabled)
\r_shadow_realtime_world_shadows\Enable rendering of shadows from realtime world lights (default: disabled)
\r_shadow_usenormalmap\Enable use of directional shading on textures (default: enabled)
\r_showsurfaces\Disable textures completely for very slow hardware. This gives a huge performance boost, but looks very ugly. (default: disabled)
\r_glsl_offsetmapping\Offset mapping effect that will make textures with bumpmaps appear like they "pop out" of the flat 2D surface (default: disabled)
\r_glsl_offsetmapping_reliefmapping\Higher quality offset mapping, which also has a huge impact on performance (default: disabled)
\r_water\Reflection and refraction quality, has a huge impact on performance on maps with reflecting surfaces (default: disabled)
\r_water_resolutionmultiplier\Resolution of reflections/refractions (default: good)
\r_coronas\Enable corona flares around certain lights (default: enabled)
\r_coronas_occlusionquery\Fade coronas according to visibility (default: enabled)
\r_bloom\Enable bloom effect, which brightens the neighboring pixels of very bright pixels. Has a big impact on performance. (default: disabled)
\r_hdr\Higher quality version of bloom, which has a huge impact on performance. (default: disabled)
\r_motionblur\Motion blur strength - 0.4 recommended

\XonoticSettingsDialog/Звук\Настройки звука
\mastervolume\-
\bgmvolume\-
\snd_staticvolume\-
\snd_channel0volume\-
\snd_channel3volume\-
\snd_channel6volume\-
\snd_channel7volume\-
\snd_channel4volume\-
\snd_channel2volume\-
\snd_channel1volume\-
\snd_speed\Частота дискретизации для вывода звука
\snd_channels\Число каналов для вывода звука
\snd_swapstereo\Поменять местами правый и левый каналы
\snd_spatialization_control\Enable spatialization (blend the right and left channel slightly to decrease stereo separation a bit for headphones)
\cl_voice_directional\Enable directional voices
\cl_voice_directional_taunt_attenuation\Расстояние, с которого будут слышны насмешки
\cl_autotaunt\Автоматически насмехаться над противниками после их поражений
\cl_sound_maptime_warning\Звук предупреждения, сообщающий оставшиеся до конца состязания минуты
\cl_hitsound\Играть звук оповещения, когда выстрел достигает противника
\menu_sounds\Играть звуки при взаимодействии с меню

\XonoticSettingsDialog/Сеть\Настройки сети
\cl_movement\Включить предсказание движения на стороне клиента
\cl_nolerp\Enable network update smoothing
\shownetgraph\Показывать график размеров пакетов и других сведений
\_cl_rate\Укажите скорость вашей сети с помощью этого ползунка
\cl_netfps\Сколько пакетов посылать серверу каждую секунду
\cl_curl_maxdownloads\Предел одновременных HTTP/FTP загрузок
\cl_curl_maxspeed\Предел скорости скачивания
\cl_port\Force client to use chosen port unless it is set to 0

\XonoticSettingsDialog/Разное\Разные настройки
\menu_tooltips\Menu tooltips: disabled, standard or advanced (also shows cvar or console command bound to the menu item)
\showtime\Показывать текущее время, полезно на снимках экранов
\showdate\Показывать текущую дату, полезно на снимках экранов
\showfps\Show your rendered frames per second

\XonoticSettingsDialog/Advanced settings...\Продвинутые настройки, в которых можно подстроить каждую переменную игры
\g_friendlyfire\Доля урона, получаемого союзниками от союзников
\g_mirrordamage\Доля урона, наносимого союзнику, которая будет отражена на себя
\g_tdm_teams_override\Заменить значение по умолчанию количества команд в командных играх

\viewsize\Включить/выключить фон HUD
\cl_hidewaypoints\Показывать различные отметки, определяемые видом игры
\g_waypointsprite_scale\Множитель размера отметок
\g_waypointsprite_alpha\Управление прозрачностью отметок
\cl_shownames\Показывать имя игрока, в которого вы целитесь

\crosshair_hittest\Отключена: нет проверок перекрестья на попадание; TrueAim: размывать перекрестье, когда не наведено на стену; Враги: также увеличивать перекрестье, когда наведено на врага
